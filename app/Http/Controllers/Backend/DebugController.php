<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DebugController extends Controller
{
    public function index (){

        dd(file_get_contents('webHook_first.log',FILE_APPEND));
    }
}
