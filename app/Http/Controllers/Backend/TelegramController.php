<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\TelegramUser;
use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Api;

class TelegramController extends Controller
{
    /**
     * @throws Telegram\Bot\Exceptions\TelegramSDKException
     */

    protected $telegram_webhook;

    public function __construct()
    {

        $this->telegram_webhook = Telegram::getWebhookUpdates();
    }

    public function webhook()
    {

        $telegram = $this->telegram_webhook;

        if (isset($telegram['callback_query'])) {
            if (!TelegramUser::find($telegram['callback_query']['from']['id'])) {
                TelegramUser::create($telegram['callback_query']['from']);
            }
        } else {
            if (!TelegramUser::find($telegram['message']['from']['id'])) {
                TelegramUser::create($telegram['message']['from']);
            }
        }


        $this->checkCommand();
        sleep(1);
        $this->switchCommand();

        Telegram::commandsHandler(true);


    }

    /**
     *
     */
    public function switchCommand()
    {
        $telegram = $this->telegram_webhook;
        $command  = $this->getLastCommand();
        file_put_contents('webHook_first.log', '--Swtch----'.$command."------\n\n", FILE_APPEND);

        if (isset($telegram['callback_query'])) {
            if ($telegram['callback_query']['data'] == $command) {
                if ($command == 'SetData' || $command == 'Указать' || $command == 'ResetData') {

                    $this->setDataCommand();
                } elseif ($command == '/start') {
                    $this->startCommand();
                }
            }
        } else {
            if ($telegram['message']['text'] == $command) {
                {

                }
            } else {
//                \Telegram::sendMessage([
//                    'chat_id' => $telegram['message']['chat']['id'],
//                    'text'    => 'Вы ввели не сущетсвующую команду2',
//                ]);
                if ($command == 'SetData' || $command == 'Указать' || $command == 'ResetData') {
                    $this->setDataCommand();
                }
            }


        };
//            if ($telegram['message']['text'] == $command) {
//                if ($command == 'SetData') {
//                    \Telegram::sendMessage([
//                        'chat_id' => $telegram['message']['chat']['id'],
//                        'text'    => 'Ранее была выбрана команда Указать данные',
//                    ]);
//                } elseif ($command == '/start') {
//                    \Telegram::sendMessage([
//                        'chat_id' => $telegram['message']['chat']['id'],
//                        'text'    => 'Ранее была выбрана команда Главное меню',
//                    ]);
//                }
//            } else {
//                \Telegram::sendMessage([
//                    'chat_id' => $telegram['message']['chat']['id'],
//                    'text'    => 'Вы ввели не сущетсвующую команду',
//                ]);
//            }

    }


    public function checkCommand()
    {
        $telegram = $this->telegram_webhook;
        if (isset($telegram['callback_query'])) {
            if ($this->getLastCommand() == null) {
                \Telegram::sendMessage([
                    'chat_id' => $telegram['callback_query']['chat']['id'],
                    'text'    => 'Ошибка - поле last command empty',
                ]);
            } else {
                if ($telegram['callback_query']['data'] == 'SetData') {
                    $this->saveLastCommand();
                } elseif ($telegram['callback_query']['data'] == '/start') {
                    $this->saveLastCommand();
                } elseif ($telegram['callback_query']['data'] == 'Указать') {
                    $this->saveLastCommand();
                } elseif ($telegram['callback_query']['data'] == 'ResetData') {
                    $this->saveLastCommand();
                }
            }
        }

    }

    public function setDataCommand()
    {
        $telegram = $this->telegram_webhook;
        //    file_put_contents('webHook_first.log', '------'.$telegram."------\n\n", FILE_APPEND);
        if (isset($telegram['callback_query'])) {
            $user    = TelegramUser::find($telegram['callback_query']['from']['id']);
            $chat_id = $telegram['callback_query']['message']['chat']['id'];
        } else {
            $user    = TelegramUser::find($telegram['message']['from']['id']);
            $chat_id = $telegram['message']['chat']['id'];
        }

        if ($user->json == null) {
            $arr        = [
                'name'  => 'null',
                'phone' => 'null'

            ];
            $user->json = json_encode($arr, true);
            $user->save();
        }
        $array = json_decode($user->json, true);
        file_put_contents('webHook_first.log', 'setDatacomand------'.var_export($array, true)."------\n\n",
            FILE_APPEND);

        $data = $this->getLastCommand();

        switch ($data) {
            case "Указать":

                if ($array['name'] != 'null') {
                    if ($array['phone'] != 'null') {
                        $param    = [
                            'inline_keyboard' => [
                                [
                                    ['text' => "Стереть данные", 'callback_data' => "ResetData"],
                                    ['text' => "Главное меню", 'callback_data' => "/start"]
                                ]
                            ],
                        ];
                        $response = \Telegram::sendMessage([
                            'chat_id'      => $chat_id,
                            'text'         => 'Всё отлично, больше нет необходимых данных '.'Ваше указанное имя: '
                                              .$array['name'].' И указанный номер телефона : '.$array['phone']
                                              .' При необходимости вы можете ввести данные повторно. ',
                            'reply_markup' => $this->inlineKeyBoard($param)
                        ]);
                    } else {


                        if (isset($telegram['message']['text'])
                            && $user->update_id != Telegram::getWebhookUpdates()['update_id']
                        ) {

                            $array['phone'] = $telegram['message']['text'];
                            $user->json     = json_encode($array, true);
                            $user->save();
                            $this->setDataCommand();
                        }
                        if ($array['phone'] == 'null') {
                            $response = \Telegram::sendMessage([
                                'chat_id' => $chat_id,
                                'text'    => 'Пожалуйста, укажите ваш номер телефона'
                            ]);
                        }
                    }
                } else {
                    file_put_contents('webHook_first.log',
                        'setDatacomand------'.Telegram::getWebhookUpdates()['update_id']."------\n\n", FILE_APPEND);
                    if (isset($telegram['message']['text'])) {

                        $array['name']   = $telegram['message']['text'];
                        $user->json      = json_encode($array, true);
                        $user->update_id = Telegram::getWebhookUpdates()['update_id'];
                        $user->save();
                        $this->setDataCommand();
                    }
                    if ($array['name'] == 'null') {
                        $response = \Telegram::sendMessage([
                            'chat_id' => $chat_id,
                            'text'    => 'Пожалуйста, укажите ваше имя'
                        ]);
                    }

                }
                break;
            case "ResetData":

                $arr        = [
                    'name'  => 'null',
                    'phone' => 'null'

                ];
                $user->json = json_encode($arr, true);
                $user->save();
                $response = \Telegram::sendMessage([
                    'chat_id' => $chat_id,
                    'text'    => 'Данные стёрты'

                ]);
                $param = [
                    'inline_keyboard' => [
                        [
                            ['text' => "Указать данные", 'callback_data' => "Указать"],
                            ['text' => "Главное меню", 'callback_data' => "/start"]
                        ]
                    ],
                ];

                $response = \Telegram::sendMessage([
                    'chat_id'      => $chat_id,
                    'text'         => 'Пожалуйста укажите ваше имя и номер телефона',
                    'reply_markup' => $this->inlineKeyBoard($param)
                ]);

                break;
            default:
                $param = [
                    'inline_keyboard' => [
                        [
                            ['text' => "Указать данные", 'callback_data' => "Указать"],
                            ['text' => "Главное меню", 'callback_data' => "/start"]
                        ]
                    ],
                ];

                $response = \Telegram::sendMessage([
                    'chat_id'      => $chat_id,
                    'text'         => 'Пожалуйста укажите ваше имя и номер телефона',
                    'reply_markup' => $this->inlineKeyBoard($param)
                ]);
        }

        //  file_put_contents('webHook_first.log', '------'.$name."------\n\n", FILE_APPEND);

        // $array = json_decode($user->json, true);

    }

    public function test()
    {
        $keyboard = [
            'inline_keyboard' => [
                [['text' => "Да", 'callback_data' => "Yes"], ['text' => "Ещё раз да", 'callback_data' => "Yes"]],
                [['text' => "Нет", 'callback_data' => "No"]],
                [['text' => "Навэльный", 'callback_data' => "navalny"]]
            ],
        ];
        $markup   = json_encode($keyboard, true);
        $telegram = Telegram::getWebhookUpdates();
        $message  = $telegram->getMessage()->gettext();
        $chat_ID  = $telegram->getMessage()->getChat()->getid();

        if (isset($telegram['callback_query'])) {
            $callback_query = $telegram['callback_query'];
            $data           = $callback_query['data'];
            //   file_put_contents('webHook_first.log', '------'.$data."------\n\n", FILE_APPEND);
        } else {
            switch ($message) {
                case 'ok':
                    \Telegram::sendMessage([
                        'chat_id'      => $chat_ID,
                        'text'         => 'Нажми уже что нибудь',
                        'reply_markup' => $markup
                    ]);
                    break;
                default:
                    if ($message != '/start') {
                        \Telegram::sendMessage([
                            'chat_id'      => $chat_ID,
                            'text'         => 'Ты не нажал ни чего',
                            'reply_markup' => $markup
                        ]);
                    }
            }
        }

        if (isset($telegram['callback_query'])) {
            $callback_query = $telegram['callback_query'];
            $data           = $callback_query['data'];
            switch ($data) {
                case 'Yes':
                    \Telegram::sendMessage([
                        'chat_id'      => $chat_ID,
                        'text'         => 'Ты нажал ДАААААа',
                        'reply_markup' => $markup
                    ]);

                    break;
                case 'No':
                    \Telegram::sendMessage([
                        'chat_id'      => $chat_ID,
                        'text'         => 'Ты нажал Нееет',
                        'reply_markup' => $markup
                    ]);
                    break;
                case 'navalny':

                    \Telegram::sendMessage([
                        'chat_id' => $chat_ID,
                        'text'    => '......................',
                    ]);

                    $action = new Api(\Telegram::getAccessToken());
                    //Бот печатает
                    $action->sendChatAction(
                        [
                            'chat_id' => $chat_ID,
                            'action'  => Actions::TYPING
                        ]
                    );
                    sleep(10);
                    \Telegram::sendMessage([
                        'chat_id' => $chat_ID,
                        'text'    => 'охохо да ты опасен',
                    ]);
                    $action = new Api(\Telegram::getAccessToken());
                    //Бот печатает
                    $action->sendChatAction(
                        [
                            'chat_id' => $chat_ID,
                            'action'  => Actions::TYPING
                        ]
                    );
                    sleep(10);
                    $photo
                              = \Telegram\Bot\FileUpload\InputFile::create('https://cdn1.img.inosmi.ru/images/24150/63/241506342.jpg');
                    $telegram = new Api(\Telegram::getAccessToken());


                    $telegram->sendPhoto([
                        'chat_id' => $chat_ID,
                        'photo'   => $photo,

                    ]);
                    break;
            }
        }

        //file_put_contents('webHook_first.log', '------'.$telegram."------\n\n", FILE_APPEND);
        file_put_contents('webHook_first.log', 'Message is: ------'.$message."------\n\n", FILE_APPEND);
    }

    public function getLastCommand()
    {
        $telegram = $this->telegram_webhook;
        if (isset($telegram['callback_query'])) {

            $user = TelegramUser::find($telegram['callback_query']['from']['id']);
            return $user->last_command;
        } else {

            $user = TelegramUser::find($telegram['message']['from']['id']);
            return $user->last_command;
        }
    }

    public function saveLastCommand()
    {
        $telegram = $this->telegram_webhook;
        if (isset($telegram['callback_query'])) {
            $user               = TelegramUser::find($telegram['callback_query']['from']['id']);
            $last_command       = $telegram['callback_query']['data'];
            $user->last_command = $last_command;
            $user->save();
        } else {
            $user               = TelegramUser::find($telegram['message']['from']['id']);
            $last_command       = $telegram['message']['text'];
            $user->last_command = $last_command;
            $user->save();
        }


    }

    public function inlineKeyBoard($param)
    {
        $keyboard = $param;
        $markup   = json_encode($keyboard, true);
        return $markup;
    }

    /**
     * @throws Telegram\Bot\Exceptions\TelegramSDKException
     */
    public function startCommand()
    {
        $telegram_user = $this->telegram_webhook;
        // file_put_contents('webHook_first.log', '------'.var_export($telegram_user['callback_query']['message']['chat']['id'],true)."------\n\n", FILE_APPEND);
        $chat_ID = $telegram_user['callback_query']['message']['chat']['id'];

        $this->saveLastCommand();
        $action = new Api(\Telegram::getAccessToken());
        //Бот печатает
        $action->sendChatAction(
            [
                'chat_id' => $chat_ID,
                'action'  => Actions::TYPING
            ]
        );
/////////////////////////////////
        $param    = [
            'inline_keyboard' => [
                [
                    ['text' => "SetData", 'callback_data' => "SetData"],
                    ['text' => "SetData2", 'callback_data' => "SetData2"]
                ],
                [
                    ['text' => "Главное меню", 'callback_data' => "/start"]
                ]
            ],
        ];
        $response = \Telegram::sendMessage([
            'chat_id'      => $chat_ID,
            'text'         => 'Добро пожаловать. Для начала работы будь добр выбери что нибудь, да или нет, всё просто',
            'reply_markup' => $this->inlineKeyBoard($param)
        ]);
    }
}
