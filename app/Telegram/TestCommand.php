<?php

namespace App\Telegram;

use App\Http\Controllers\Backend\TelegramController;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

/**
 * Class HelpCommand.
 */
class TestCommand extends Command
{
    protected $name = 'start';

    protected $description = 'H123';

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $telegram_user = \Telegram::getWebhookUpdates();
        $chat_ID       = $telegram_user->getMessage()->getChat()->getid();

        $telegram_controller = new TelegramController();
        $telegram_controller->saveLastCommand();

        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $param    = [
            'inline_keyboard' => [
                [
                    ['text' => "SetData", 'callback_data' => "SetData"],
                    ['text' => "SetData2", 'callback_data' => "SetData2"]
                ],
                [
                    ['text' => "Главное меню", 'callback_data' => "/start"]
                ]
            ],
        ];
        $response = \Telegram::sendMessage([
            'chat_id'      => $chat_ID,
            'text'         => 'Добро пожаловать. Для начала работы будь добр выбери что нибудь, да или нет, всё просто',
            'reply_markup' => $telegram_controller->inlineKeyBoard($param)
        ]);

    }
}