<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TelegramUser
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TelegramUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TelegramUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TelegramUser query()
 * @mixin \Eloquent
 */
class TelegramUser extends Model
{
    protected $guarded = [];
}
