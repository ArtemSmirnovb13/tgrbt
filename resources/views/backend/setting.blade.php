@extends('backend.layouts.app')

@section('content')
    @if (Session::has('status'))
        <div class="alert alert-info">
            <span>{{Session::get('status')}}</span>
        </div>
    @endif
    <div class="container">
        <form action="{{route('admin.setting.store')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label>Url callback для телеграм</label>
                <div class="input-group">
                    <div class="input-group">

                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">action <span class="dropdown"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#" onclick="document.getElementById('url_callback_bot').value = '{{url('')}}'">Вставить
                                    URL</a></li>
                            <li><a href="#" onclick="event.preventDefault(); document.getElementById('setwebhook').submit();"> Отправить URL</a></li>
                            <li><a href="#" onclick="event.preventDefault(); document.getElementById('getwebhookinfo').submit();">Получить информацию</a></li>
                        </ul>
                        <input type="url" class="form-control" id="url_callback_bot" name="url_callback_bot"
                               value="{{  $url_callback_bot ?? ''}}">
                    </div>

                </div>
            </div>

            <div class="form-group">
                <label>Напиши что нибудь или введи имя бота</label>

                <div class="input-group">
                    <input type="text" class="form-control" name="site2" value="{{ $site2 ?? '!'}}">
                </div>

            </div>
            <button class="btn btn-success" type="submit">Сохранить</button>
        </form>
        <form id="setwebhook" action="{{route('admin.setting.setwebhook')}}" method="POST" style="display: none;">
            {{csrf_field()}}
            <input type="hidden" name="url" value="{{ $url_callback_bot ?? ''}}">
        </form>
        <form id="getwebhookinfo" action="{{route('admin.setting.getwebhookinfo')}}" method="POST" style="display: none;">
            {{csrf_field()}}
        </form>
    </div>

@endsection